﻿using System;
using MovieRental.Business.Model;


class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("## Regular");
        Console.WriteLine(PriceCode.Regular().Describe());
        Console.WriteLine();

        Console.WriteLine("## Children");
        Console.WriteLine(PriceCode.Children().Describe());
        Console.WriteLine();

        Console.WriteLine("## New Release");
        Console.WriteLine(PriceCode.NewRelease().Describe());
        Console.WriteLine();
    }
}
