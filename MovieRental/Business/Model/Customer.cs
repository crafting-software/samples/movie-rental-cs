namespace MovieRental.Business.Model;

using System.Collections.Generic;
using System.Linq;

public class Customer
{
    private string Name;
    private List<Rental> rentals = new List<Rental>();

    public Customer(string name)
    {
        this.Name = name;
    }

    public void AddRental(Rental arg)
    {
        rentals.Add(arg);
    }

    public string GetName()
    {
        return Name;
    }

    public string Statement()
    {
        return new Statement(
            GetName(),
            rentals.ConvertAll(r => new Statement.Entry(r.Title(), r.Charge())),
            rentals.Aggregate(Price.Euro(0), (price, rental) => price + rental.Charge()),
            rentals.ConvertAll(r => r.FrequentRenterPoints()).Sum()
            ).Text();
    }
}

