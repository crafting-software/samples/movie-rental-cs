namespace MovieRental.Business.Model;

using System;

public class Movie
{
    public PriceCode _priceCode;
    public String Title { get; }

    public Movie(String title, PriceCode priceCode)
    {
        Title = title;
        _priceCode = priceCode;
    }

    public Price ChargeFor(int daysRented)
    {
        return _priceCode.ChargeFor(daysRented);
    }

    public int FrequentRenterPointsFor(int daysRented)
    {
        return _priceCode.FrequentRenterPointsFor(daysRented);
    }
}
