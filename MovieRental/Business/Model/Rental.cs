namespace MovieRental.Business.Model;

public class Rental
{
    private Movie _movie;
    private int _daysRented;

    public Rental(Movie movie, int daysRented)
    {
        _movie = movie;
        _daysRented = daysRented;
    }

    public string Title()
    {
        return _movie.Title;
    }

    public Price Charge()
    {
        return _movie.ChargeFor(_daysRented);
    }

    public int FrequentRenterPoints()
    {
        return _movie.FrequentRenterPointsFor(_daysRented);
    }
}
