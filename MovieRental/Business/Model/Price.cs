namespace MovieRental.Business.Model;

using System;
using System.Collections.Generic;

public record Price(double Amount)
{
    public static Price Euro(double value) => new Price(value);

    public static Price operator +(Price x, Price y) => new Price(x.Amount + y.Amount);

    public static Price operator *(Price x, double factor) => new Price(x.Amount * factor);

    public override string ToString() => $"{Amount}€";
}
