﻿namespace MovieRental.Business.Model;

using System;
using System.Collections.Generic;

public struct Duration : IEquatable<Duration>, IComparable<Duration>, IComparable
{
    private const int InfiniteValue = -1;

    private Duration(int amount)
    {
        Value = amount;
    }

    public int Value { get; }

    public int CompareTo(object obj)
    {
        if (ReferenceEquals(null, obj))
        {
            return 1;
        }

        if (!(obj is Duration))
        {
            throw new ArgumentException($"Object must be of type {nameof(Duration)}");
        }

        return CompareTo((Duration)obj);
    }


    public int CompareTo(Duration other)
    {
        if (IsInfinite())
        {
            return other.IsInfinite() ? 0 : 1;
        }

        if (other.IsInfinite())
        {
            return -1;
        }

        return Value.CompareTo(other.Value);
    }

    public bool Equals(Duration other)
    {
        return Value == other.Value;
    }


    public static Duration Infinite() => new Duration(InfiniteValue);
    public static Duration Null() => new Duration(0);

    public static Duration Days(int value) => new Duration(value);

    public bool IsInfinite() => Value == InfiniteValue;
    public bool IsNotInfinite() => Value != InfiniteValue;
    public bool IsNull() => Value == 0;
    public bool IsNotNull() => Value != 0;

    public static Duration operator +(Duration left, Duration right)
    {
        if (left.IsInfinite() || right.IsInfinite())
        {
            return Infinite();
        }

        return Days(left.Value + right.Value);
    }

    public static Duration operator -(Duration left, Duration right)
    {
        if (left.IsInfinite())
        {
            return right.IsInfinite() ? Null() : Infinite();
        }

        if (right.IsInfinite())
        {
            return Null();
        }

        return Days(Math.Max(left.Value - right.Value, 0));
    }

    public static bool operator ==(Duration left, Duration right) => Equals(left, right);

    public static bool operator !=(Duration left, Duration right) => !Equals(left, right);

    public static bool operator <(Duration left, Duration right) =>
        Comparer<Duration>.Default.Compare(left, right) < 0;

    public static bool operator >(Duration left, Duration right) =>
        Comparer<Duration>.Default.Compare(left, right) > 0;

    public static bool operator <=(Duration left, Duration right) =>
        Comparer<Duration>.Default.Compare(left, right) <= 0;

    public static bool operator >=(Duration left, Duration right) =>
        Comparer<Duration>.Default.Compare(left, right) >= 0;


    public static Duration Max(Duration left, Duration right)
    {
        if (left.IsInfinite())
        {
            return Infinite();
        }

        if (right.IsInfinite())
        {
            return Infinite();
        }

        return Days(Math.Max(left.Value, right.Value));
    }

    public static Duration Min(Duration left, Duration right)
    {
        if (left.IsInfinite())
        {
            return right;
        }

        if (right.IsInfinite())
        {
            return left;
        }

        return Days(Math.Min(left.Value, right.Value));
    }

    public override string ToString() => IsInfinite() ? "Duration: infinite" : $"Duration: {Value}";

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj))
        {
            return false;
        }

        return obj.GetType() == GetType() && Equals((Duration)obj);
    }

    public override int GetHashCode() => Value;
}
