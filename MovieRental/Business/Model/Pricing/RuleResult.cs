namespace MovieRental.Business.Model.Pricing;

using System;
using System.Collections.Generic;

public record RuleResult(Price price, Duration remaining)
{
    public static RuleResult Of(Price amount)
    {
        return new RuleResult(amount, Duration.Null());
    }
}
