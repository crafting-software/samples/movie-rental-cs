namespace MovieRental.Business.Model.Pricing;


public class PricingFactory
{
    public static Rule Flat(Price amount) => new Rule.FlatRule(amount);
    public static Rule PerDay(Price amount) => new Rule.ProportionalRule(amount);
    public static Rule Limit(Rule rule, Duration limit) => new Rule.LimitedRule(rule, limit);
    public static Rule And(Rule left, Rule right) => new Rule.CombinedRule(left, right);

}
