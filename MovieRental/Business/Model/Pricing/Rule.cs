namespace MovieRental.Business.Model.Pricing;

using System;
using System.Collections.Generic;

public abstract record Rule
{
    public abstract RuleResult PriceFor(Duration duration);
    public abstract String Describe();

    public record FlatRule(Price amount) : Rule
    {
        public override RuleResult PriceFor(Duration duration)
        {
            return RuleResult.Of(amount);
        }
        public override String Describe() => $"cost {amount}";
    }
    public record ProportionalRule(Price amount) : Rule
    {
        public override RuleResult PriceFor(Duration duration)
        {
            return RuleResult.Of(amount * duration.Value);
        }
        public override String Describe() => $"cost {amount} per day";
    }
    public record LimitedRule(Rule inner, Duration limit) : Rule
    {
        public override RuleResult PriceFor(Duration duration)
        {
            return inner.PriceFor(Duration.Min(duration, limit))
            with
            {
                remaining = duration - Duration.Min(duration, limit)
            };
        }
        public override String Describe()
            => $"{inner.Describe()} for {limit.Value} day(s)";
    }
    public record CombinedRule(Rule left, Rule right) : Rule
    {
        public override RuleResult PriceFor(Duration duration)
        {
            var first = left.PriceFor(duration);
            var second = right.PriceFor(first.remaining);
            return new RuleResult(first.price + second.price, second.remaining);
        }
        public override String Describe()
            => $"{left.Describe()} then {right.Describe()}";
    }
}
