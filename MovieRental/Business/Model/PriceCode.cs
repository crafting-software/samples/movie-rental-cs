namespace MovieRental.Business.Model;

using System;
using MovieRental.Business.Model.Pricing;

public abstract record PriceCode
{
    public sealed record RegularCode() : PriceCode()
    {
        private static readonly Rule rule = PricingFactory.And(
            PricingFactory.Limit(
                PricingFactory.Flat(Price.Euro(2)),
                Duration.Days(2)),
            PricingFactory.PerDay(Price.Euro(1.5))
        );
        public override Price ChargeFor(int daysRented)
        {
            return rule.PriceFor(Duration.Days(daysRented)).price;
        }
        public override int FrequentRenterPointsFor(int daysRented)
        {
            return 1;
        }
        public override String Describe() => rule.Describe();

    }

    public sealed record NewReleaseCode() : PriceCode()
    {
        private static readonly Rule rule = PricingFactory.PerDay(Price.Euro(3));

        public override Price ChargeFor(int daysRented)
        {
            return rule.PriceFor(Duration.Days(daysRented)).price;
        }
        public override int FrequentRenterPointsFor(int daysRented)
        {
            return daysRented > 1 ? 2 : 1;
        }
        public override String Describe() => rule.Describe();
    }

    public sealed record ChildrenCode() : PriceCode()
    {
        private static readonly Rule rule = PricingFactory.And(
            PricingFactory.Limit(
                PricingFactory.Flat(Price.Euro(1.5)),
                Duration.Days(3)),
            PricingFactory.PerDay(Price.Euro(1.5))
        );
        public override Price ChargeFor(int daysRented)
        {
            return rule.PriceFor(Duration.Days(daysRented)).price;
        }
        public override int FrequentRenterPointsFor(int daysRented)
        {
            return 1;
        }
        public override String Describe() => rule.Describe();
    }

    public static PriceCode Regular()
    {
        return new PriceCode.RegularCode();
    }

    public static PriceCode NewRelease()
    {
        return new PriceCode.NewReleaseCode();
    }

    public static PriceCode Children()
    {
        return new PriceCode.ChildrenCode();
    }

    public abstract Price ChargeFor(int daysRented);
    public abstract int FrequentRenterPointsFor(int daysRented);

    public abstract String Describe();

    // Prevent derived cases from being defined elsewhere
    private PriceCode() { }
}
