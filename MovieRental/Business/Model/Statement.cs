namespace MovieRental.Business.Model;

using System.Collections.Generic;

public class Statement
{
    public record Entry(string label, Price price);

    private string name;
    private List<Entry> entries;
    private Price total;
    private int frequentRenterPoints;


    public Statement(string name, List<Entry> entries, Price total, int frequentRenterPoints)
    {
        this.name = name;
        this.entries = entries;
        this.total = total;
        this.frequentRenterPoints = frequentRenterPoints;
    }

    public string Text()
    {
        string result = $"Rental Record for {name}\n";
        foreach (Entry entry in entries)
        {
            result += $"\t{entry.label}\t{entry.price.Amount}\n";
        }
        result += $"Amount owed is {total.Amount}\n";
        result += $"You earned {frequentRenterPoints} frequent renter points";
        return result;
    }
}
