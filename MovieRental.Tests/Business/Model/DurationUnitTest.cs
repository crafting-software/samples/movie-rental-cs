﻿namespace MovieRental.Business.Model.Tests;

using static MovieRental.Business.Model.Duration;

public class DurationUnitTest
{
    [Test]
    public void duration_should_render()
    {
        Assert.That(Null().ToString(), Is.EqualTo("Duration: 0"));
        Assert.That(Days(5).ToString(), Is.EqualTo("Duration: 5"));
        Assert.That(Infinite().ToString(), Is.EqualTo("Duration: infinite"));
    }

    [Test]
    public void duration_should_equal()
    {
        Assert.That(Null(), Is.EqualTo(Null()));
        Assert.That(Null(), Is.Not.EqualTo(Infinite()));
        Assert.That(Null(), Is.Not.EqualTo(Days(5)));

        Assert.That(Infinite(), Is.EqualTo(Infinite()));
        Assert.That(Infinite(), Is.Not.EqualTo(Null()));
        Assert.That(Infinite(), Is.Not.EqualTo(Days(5)));

        Assert.That(Days(5), Is.EqualTo(Days(5)));
        Assert.That(Days(5), Is.Not.EqualTo(Null()));
        Assert.That(Days(5), Is.Not.EqualTo(Infinite()));
    }

    [Test]
    public void duration_should_compare()
    {
        Assert.That(Null() < Days(1), Is.True);
        Assert.That(Days(5) < Days(10), Is.True);
        Assert.That(Days(5) < Infinite(), Is.True);
    }

    [Test]
    public void duration_should_add()
    {
        Assert.That(Days(3) + Days(4), Is.EqualTo(Days(7)));
        Assert.That(Null() + Days(7), Is.EqualTo(Days(7)));
        Assert.That(Days(3) + Infinite(), Is.EqualTo(Infinite()));
        Assert.That(Infinite() + Days(3), Is.EqualTo(Infinite()));
    }

    [Test]
    public void duration_should_sub()
    {
        Assert.That(Days(7) - Days(4), Is.EqualTo(Days(3)));
        Assert.That(Days(4) - Days(7), Is.EqualTo(Null()));
        Assert.That(Days(4) - Infinite(), Is.EqualTo(Null()));
        Assert.That(Infinite() - Infinite(), Is.EqualTo(Null()));
        Assert.That(Infinite() - Days(4), Is.EqualTo(Infinite()));
    }

    [Test]
    public void duration_should_check_null()
    {
        Assert.That(Null().IsNull(), Is.True);
        Assert.That(Days(4).IsNotNull(), Is.True);
        Assert.That(Infinite().IsNotNull(), Is.True);
    }

    [Test]
    public void duration_should_check_infinite()
    {
        Assert.That(Null().IsNotInfinite(), Is.True);
        Assert.That(Days(4).IsNotInfinite(), Is.True);
        Assert.That(Infinite().IsInfinite(), Is.True);
    }

    [Test]
    public void duration_should_max()
    {
        Assert.That(Max(Days(7), Days(4)), Is.EqualTo(Days(7)));
        Assert.That(Max(Days(7), Null()), Is.EqualTo(Days(7)));
        Assert.That(Max(Infinite(), Days(5)), Is.EqualTo(Infinite()));
        Assert.That(Max(Days(7), Infinite()), Is.EqualTo(Infinite()));
        Assert.That(Max(Infinite(), Infinite()), Is.EqualTo(Infinite()));
    }

    [Test]
    public void duration_should_min()
    {
        Assert.That(Min(Days(7), Days(4)), Is.EqualTo(Days(4)));
        Assert.That(Min(Days(7), Null()), Is.EqualTo(Null()));
        Assert.That(Min(Infinite(), Days(5)), Is.EqualTo(Days(5)));
        Assert.That(Min(Days(7), Infinite()), Is.EqualTo(Days(7)));
        Assert.That(Min(Infinite(), Infinite()), Is.EqualTo(Infinite()));
    }
}
