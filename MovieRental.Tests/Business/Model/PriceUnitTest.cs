﻿namespace MovieRental.Business.Model.Tests;

using static MovieRental.Business.Model.Price;

public class PriceUnitTest
{
    [Test]
    public void price_should_equals()
    {
        Assert.That(Euro(0), Is.EqualTo(Euro(0)));
    }

    [Test]
    public void price_should_compare()
    {
        Assert.That(Euro(4), Is.Not.EqualTo(Euro(3)));
    }

    [Test]
    public void price_should_render()
    {
        Assert.That(Euro(3).ToString, Is.EqualTo("3€"));
    }

    [Test]
    public void price_should_add()
    {
        Assert.That(Euro(3) + Euro(4), Is.EqualTo(Euro(7)));
    }

    [Test]
    public void price_should_mul()
    {
        Assert.That(Euro(4) * 2, Is.EqualTo(Euro(8)));
    }
}
