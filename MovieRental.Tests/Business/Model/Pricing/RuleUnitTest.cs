﻿namespace MovieRental.Business.Model.Pricing.Tests;

using MovieRental.Business.Model;
using MovieRental.Business.Model.Pricing;

public class PricingUnitTest
{
    private static Price PriceFor(Rule rule, Duration duration)
    {
        return rule.PriceFor(duration).price;
    }

    [Test]
    public void flat_rule__price()
    {
        Assert.That(
            PriceFor(new Rule.FlatRule(Price.Euro(2)), Duration.Days(5)),
            Is.EqualTo(Price.Euro(2)));
    }

    [Test]
    public void flat_rule__description()
    {
        Assert.That(
            new Rule.FlatRule(Price.Euro(2)).Describe(),
            Is.EqualTo("cost 2€"));
    }

    [Test]
    public void proportional_rule__price()
    {
        Assert.That(
            PriceFor(new Rule.ProportionalRule(Price.Euro(2)), Duration.Days(3)),
            Is.EqualTo(Price.Euro(6)));
    }

    [Test]
    public void proportional_rule__description()
    {
        Assert.That(
            new Rule.ProportionalRule(Price.Euro(2)).Describe(),
            Is.EqualTo("cost 2€ per day"));
    }

    [Test]
    public void limited_rule__price()
    {
        Assert.That(
            PriceFor(
                new Rule.LimitedRule(
                    new Rule.ProportionalRule(Price.Euro(2)), Duration.Days(3)), Duration.Days(10)),
            Is.EqualTo(Price.Euro(6)));
    }

    [Test]
    public void limited_rule__description()
    {
        var rule = new Rule.LimitedRule(
                        new Rule.ProportionalRule(Price.Euro(2)),
                        Duration.Days(3));
        Assert.That(
            rule.Describe(),
            Is.EqualTo("cost 2€ per day for 3 day(s)"));
    }

    [Test]
    public void combined_rule__price()
    {
        Assert.That(
            PriceFor(
                new Rule.CombinedRule(
                    new Rule.LimitedRule(
                        new Rule.FlatRule(Price.Euro(2)),
                        Duration.Days(3)),
                    new Rule.ProportionalRule(Price.Euro(2))
                ),
                Duration.Days(10)),
            Is.EqualTo(Price.Euro(2 + 2 * 7)));
    }

    [Test]
    public void combined_rule__description()
    {
        var rule = new Rule.CombinedRule(
                    new Rule.LimitedRule(
                        new Rule.FlatRule(Price.Euro(2)),
                        Duration.Days(3)),
                    new Rule.ProportionalRule(Price.Euro(4))
                );
        Assert.That(
            rule.Describe(),
            Is.EqualTo("cost 2€ for 3 day(s) then cost 4€ per day"));
    }
}
