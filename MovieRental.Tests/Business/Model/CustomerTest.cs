namespace MovieRental.Business.Model.Tests;

using System;
using MovieRental.Business.Model;

public class CustomerTest
{
    [Test]
    public void Test1()
    {
        Customer customer = new Customer("Bob");
        customer.AddRental(new Rental(new Movie("Jaws", PriceCode.Regular()), 2));
        customer.AddRental(new Rental(new Movie("Short New", PriceCode.NewRelease()), 1));
        customer.AddRental(new Rental(new Movie("Long New", PriceCode.NewRelease()), 2));
        customer.AddRental(new Rental(new Movie("Toy Story", PriceCode.Children()), 4));

        String expected = "" +
            "Rental Record for Bob\n" +
            "\tJaws\t2\n" +
            "\tShort New\t3\n" +
            "\tLong New\t6\n" +
            "\tToy Story\t3\n" +
            "Amount owed is 14\n" +
            "You earned 5 frequent renter points";

        Assert.That(customer.Statement(), Is.EqualTo(expected));
    }

    [Test]
    public void statement_with_regular_movie()
    {
        var customer = new Customer("jpartogi");
        customer.AddRental(new Rental(new Movie("Transformer", PriceCode.Regular()), 5));
        Assert.That(customer.Statement(), Is.EqualTo(
            "Rental Record for jpartogi\n" +
            "\tTransformer\t6,5\n" +
            "Amount owed is 6,5\n" +
            "You earned 1 frequent renter points"));
    }

    [Test]
    public void statement_with_children_movie()
    {
        var customer = new Customer("jpartogi");
        customer.AddRental(new Rental(new Movie("Bambi", PriceCode.Children()), 5));
        Assert.That(customer.Statement(), Is.EqualTo(
            "Rental Record for jpartogi\n" +
            "\tBambi\t4,5\n" +
            "Amount owed is 4,5\n" +
            "You earned 1 frequent renter points"));
    }

    [Test]
    public void statement_with_new_release_movie()
    {
        var customer = new Customer("jpartogi");
        customer.AddRental(new Rental(new Movie("New one", PriceCode.NewRelease()), 5));
        Assert.That(customer.Statement(), Is.EqualTo(
            "Rental Record for jpartogi\n" +
            "\tNew one\t15\n" +
            "Amount owed is 15\n" +
            "You earned 2 frequent renter points"));
    }

    [Test]
    public void statement_with_differents_movies()
    {
        var customer = new Customer("jpartogi");
        customer.AddRental(new Rental(new Movie("New one", PriceCode.NewRelease()), 5));
        customer.AddRental(new Rental(new Movie("Transformer", PriceCode.Regular()), 5));
        Assert.That(customer.Statement(), Is.EqualTo(
            "Rental Record for jpartogi\n" +
            "\tNew one\t15\n" +
            "\tTransformer\t6,5\n" +
            "Amount owed is 21,5\n" +
            "You earned 3 frequent renter points"));
    }
}
